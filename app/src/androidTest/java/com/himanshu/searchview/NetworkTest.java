package com.himanshu.searchview;

import com.himanshu.searchview.di.module.NetworkModule;
import com.himanshu.searchview.gateway.model.SearchNetworkResponse;
import com.himanshu.searchview.network.NetworkConstants;
import com.himanshu.searchview.network.SearchApi;
import com.himanshu.searchview.ui.BaseActivity;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;

public class NetworkTest extends BaseActivity {

    @Inject
    SearchApi searchApi;



    /*
    Failure due to Lopper thread issue
     */
    @Test
    public  void testAPIEnd(){

        ((SearchApplication)SearchApplication.Companion.getContext()).getComponent().inject(this);
        Observable<SearchNetworkResponse> response=searchApi.searchItem(NetworkConstants.INSTANCE.getCLIENT_ID(),NetworkConstants.INSTANCE.getCLIENT_SECRET(),NetworkConstants.INSTANCE.getNEAR(),"coffee","20180401",20);
        response.subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<SearchNetworkResponse>() {
            @Override
            public void onNext(SearchNetworkResponse searchNetworkResponse) {
                Assert.assertTrue(searchNetworkResponse!=null);
            }

            @Override
            public void onError(Throwable e) {
             Assert.assertFalse(e==null);
            }

            @Override
            public void onComplete() {

            }
        });


    }
}
