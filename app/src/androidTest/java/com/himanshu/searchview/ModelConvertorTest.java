package com.himanshu.searchview;

import com.google.gson.Gson;
import com.himanshu.searchview.gateway.ModelConvertor;
import com.himanshu.searchview.gateway.model.SearchNetworkResponse;
import org.junit.Test;

public class ModelConvertorTest {
    
    
    String sampleJson="{" +
            "    \"meta\": {" +
            "        \"code\": 200," +
            "        \"requestId\": \"5cef98c44434b92155592f61\"" +
            "    }," +
            "    \"response\": {" +
            "        \"venues\": [" +
            "            " +
            "            " +
            "            " +
            "            {" +
            "                \"id\": \"5c5492f20a08ab002c82df3f\"," +
            "                \"name\": \"Retro Coffee\"," +
            "                \"contact\": {}," +
            "                \"location\": {" +
            "                    \"lat\": 47.606662," +
            "                    \"lng\": -122.331461," +
            "                    \"labeledLatLngs\": [" +
            "                        {" +
            "                            \"label\": \"display\"," +
            "                            \"lat\": 47.606662," +
            "                            \"lng\": -122.331461" +
            "                        }" +
            "                    ]," +
            "                    \"postalCode\": \"98104\"," +
            "                    \"cc\": \"US\"," +
            "                    \"city\": \"Seattle\"," +
            "                    \"state\": \"WA\"," +
            "                    \"country\": \"United States\"," +
            "                    \"formattedAddress\": [" +
            "                        \"Seattle, WA 98104\"," +
            "                        \"United States\"" +
            "                    ]" +
            "                }," +
            "                \"categories\": [" +
            "                    {" +
            "                        \"id\": \"4bf58dd8d48988d1e0931735\"," +
            "                        \"name\": \"Coffee Shop\"," +
            "                        \"pluralName\": \"Coffee Shops\"," +
            "                        \"shortName\": \"Coffee Shop\"," +
            "                        \"icon\": {" +
            "                            \"prefix\": \"https://ss3.4sqi.net/img/categories_v2/food/coffeeshop_\"," +
            "                            \"suffix\": \".png\"" +
            "                        }," +
            "                        \"primary\": true" +
            "                    }" +
            "                ]," +
            "                \"verified\": false," +
            "                \"stats\": {" +
            "                    \"tipCount\": 0," +
            "                    \"usersCount\": 0," +
            "                    \"checkinsCount\": 0," +
            "                    \"visitsCount\": 0" +
            "                }," +
            "                \"beenHere\": {" +
            "                    \"count\": 0," +
            "                    \"lastCheckinExpiredAt\": 0," +
            "                    \"marked\": false," +
            "                    \"unconfirmedCount\": 0" +
            "                }," +
            "                \"hereNow\": {" +
            "                    \"count\": 0," +
            "                    \"summary\": \"Nobody here\"," +
            "                    \"groups\": []" +
            "                }," +
            "                \"referralId\": \"v-1559206084\"," +
            "                \"venueChains\": []," +
            "                \"hasPerk\": false" +
            "            }" +
            "            " +
            "        ]," +
            "        \"geocode\": {" +
            "            \"what\": \"\"," +
            "            \"where\": \"seattle wa\"," +
            "            \"feature\": {" +
            "                \"cc\": \"US\"," +
            "                \"name\": \"Seattle\"," +
            "                \"displayName\": \"Seattle, WA, United States\"," +
            "                \"matchedName\": \"Seattle, WA, United States\"," +
            "                \"highlightedName\": \"<b>Seattle</b>, <b>WA</b>, United States\"," +
            "                \"woeType\": 7," +
            "                \"slug\": \"seattle-washington\"," +
            "                \"id\": \"geonameid:5809844\"," +
            "                \"longId\": \"72057594043737780\"," +
            "                \"geometry\": {" +
            "                    \"center\": {" +
            "                        \"lat\": 47.60621," +
            "                        \"lng\": -122.33207" +
            "                    }," +
            "                    \"bounds\": {" +
            "                        \"ne\": {" +
            "                            \"lat\": 47.734145," +
            "                            \"lng\": -122.224433" +
            "                        }," +
            "                        \"sw\": {" +
            "                            \"lat\": 47.481719999999996," +
            "                            \"lng\": -122.459696" +
            "                        }" +
            "                    }" +
            "                }" +
            "            }," +
            "            \"parents\": []" +
            "        }" +
            "    }" +
            "}";


    @Test
    public void givesListWhenJsonNull(){
        assert (ModelConvertor.Companion.convertSearchResults(null)!=null);
    }

    @Test
    public void givesListWhenSomeValuesAreNull(){
        SearchNetworkResponse response= new Gson().fromJson(sampleJson,SearchNetworkResponse.class);
        assert (ModelConvertor.Companion.convertSearchResults(response)==null);
    }
}
