package com.himanshu.searchview.gateway

import com.himanshu.searchview.network.Response
import com.himanshu.searchview.ui.model.SearchListArrayModel
import io.reactivex.Observable

open interface ISearchDataSource{


    fun getSearchResults(queryText:String): Observable<Response<SearchListArrayModel>>
}