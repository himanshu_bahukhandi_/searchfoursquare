package com.himanshu.searchview.gateway.cache;

import android.text.TextUtils;
import android.util.LruCache;

public class JsonCache {

    private static JsonCache cache;
    private LruCache<String,String> mMemoryMap;
    private JsonCache(){

    }


    public static JsonCache getInstance(){
        if(cache==null){
            cache=new JsonCache();
            int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            int cacheSize = maxMemory / 8 ;
            cache.mMemoryMap=new LruCache<String, String>(cacheSize);



        }
        return cache;
    }


    public String getJson(String key){
        if(TextUtils.isEmpty(key)){
            return "";
        }
        if(mMemoryMap.get(key)==null){
            return "";
        }
        return mMemoryMap.get(key);
    }


    public void setJson(String key,String json)
    {
        if(TextUtils.isEmpty(key) || TextUtils.isEmpty(json)){
            return;
        }
        mMemoryMap.put(key,json);
    }


}
