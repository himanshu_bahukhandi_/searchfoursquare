package com.himanshu.searchview.gateway.remote

import com.himanshu.searchview.SearchApplication
import com.himanshu.searchview.gateway.ISearchDataSource
import com.himanshu.searchview.gateway.ModelConvertor
import com.himanshu.searchview.gateway.model.SearchNetworkResponse
import com.himanshu.searchview.network.NetworkConstants
import com.himanshu.searchview.network.Response
import com.himanshu.searchview.network.SearchApi
import com.himanshu.searchview.ui.model.SearchListArrayModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.text.SimpleDateFormat
import java.util.function.Consumer
import javax.inject.Inject
import kotlin.Exception

open class SearchRemoteDataSource : ISearchDataSource {

    @Inject
    public lateinit var searchAPI: SearchApi

    @Inject
    constructor(){
        (SearchApplication.context as SearchApplication).getComponent().inject(this)
    }

//    @Inject
//    constructor(){
//     SearchApplication.
//    }


    override fun getSearchResults(queryText: String): Observable<Response<SearchListArrayModel>> {

        val mObservable = PublishSubject.create<Response<SearchListArrayModel>>()

        var networkObservable:Observable<SearchNetworkResponse> = searchAPI!!.searchItem(NetworkConstants.CLIENT_ID,NetworkConstants.CLIENT_SECRET,NetworkConstants.NEAR,queryText,"20180401",20)

        val subscribe = networkObservable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
            handleResponse(it,mObservable)
        },{

            })
        return mObservable
    }


    private fun handleResponse(searchResponse:SearchNetworkResponse,mObservable:PublishSubject<Response<SearchListArrayModel>>){
        var modellist= ModelConvertor.convertSearchResults(searchResponse)

        if(modellist?.searchList!!.size>0){
            mObservable.onNext(Response.success(modellist!!))
        }else{
            mObservable.onNext(Response.error(Exception("error"),searchResponse as retrofit2.Response<SearchListArrayModel>,SearchListArrayModel::class.java))
        }
    }

    private fun handleError(t:Throwable,mObservable:PublishSubject<Response<SearchListArrayModel>>) {
        mObservable.onNext(Response.error())

    }


}