package com.himanshu.searchview.gateway.local

import android.text.TextUtils
import com.google.gson.Gson
import com.himanshu.searchview.gateway.ISearchDataSource
import com.himanshu.searchview.gateway.cache.JsonCache
import com.himanshu.searchview.network.Response
import com.himanshu.searchview.ui.model.SearchListArrayModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.Callable


open class SearchLocalDataSource:ISearchDataSource{
    override fun getSearchResults(queryText: String): Observable<Response<SearchListArrayModel>> {
        val mObservable = PublishSubject.create<Response<SearchListArrayModel>>()


        return Observable.fromCallable(object : Callable<Response<SearchListArrayModel>> {
            @Throws(Exception::class)
            override fun call(): Response<SearchListArrayModel> {
                val json=JsonCache.getInstance().getJson(queryText);
                if(!TextUtils.isEmpty(json)){
                    var modellist:SearchListArrayModel=Gson().fromJson(json,SearchListArrayModel::class.java)
                    //var modellist=ModelConvertor.convertSearchResults(searchResponse)
                    if(modellist?.searchList!!.size>0){
                      return Response.success(modellist!!);
                    }
            }
                return Response.error();
        }});




    }

    fun saveResultData(key:String,data: SearchListArrayModel) {

        JsonCache.getInstance().setJson(key,Gson().toJson(data))

    }

}