package com.himanshu.searchview.gateway

import android.content.Context
import android.location.Location
import com.himanshu.searchview.SearchApplication
import com.himanshu.searchview.gateway.model.SearchNetworkResponse
import com.himanshu.searchview.gateway.model.Venue
import com.himanshu.searchview.ui.model.SearchListArrayModel
import com.himanshu.searchview.ui.model.SearchListItem
import com.himanshu.searchview.ui.utils.SharedPrefUtils
import com.himanshu.searchview.ui.utils.UIConstants
import java.lang.Exception

open class ModelConvertor {

    companion object {

        fun convertSearchResults(serverResponse: SearchNetworkResponse): SearchListArrayModel? {
            var listModel = SearchListArrayModel();
            try {
                if (serverResponse != null) {
                    if (serverResponse.response?.venues != null && serverResponse.response?.venues.size > 0) {
                        for (venue: Venue? in serverResponse.response.venues) {
                            var listItem = SearchListItem()
                            var centerLatOfSeatle = serverResponse.response.geocode!!.feature!!.geometry!!.center!!.lat
                            var centerLngOfSeatle = serverResponse.response.geocode!!.feature!!.geometry!!.center!!.lng
                            SharedPrefUtils.saveData(SearchApplication.context as Context,UIConstants.CENTRE_LAT,centerLatOfSeatle.toString())
                            SharedPrefUtils.saveData(SearchApplication.context as Context,UIConstants.CENTRE_LNG,centerLngOfSeatle.toString())



                            if (venue != null) {
                                listItem.id = venue.id.toString()
                                if (venue.categories != null && venue.categories.size > 0) {
                                    listItem.name= venue.name.toString()
                                    listItem.category = venue.categories.get(0)!!.name.toString()
                                    listItem.categoryIcon =
                                        venue.categories.get(0)!!.icon!!.prefix.toString() + "32" + venue.categories.get(
                                            0
                                        )!!.icon!!.suffix.toString()
                                    if (SharedPrefUtils.getBooleanData(
                                            SearchApplication.context!!,
                                            venue!!.id.toString()
                                        )
                                    ) {
                                        listItem.isFav = true
                                    } else {
                                        listItem.isFav = false;
                                    }
                                    listItem.address = venue.location?.formattedAddress?.get(0).toString()
                                    listItem.lat = venue.location?.lat!!
                                    listItem.long = venue.location?.lng!!

                                    val distance = getDistanceBetweenTwoLocation(centerLatOfSeatle, centerLngOfSeatle, listItem.lat,listItem.long)

                                    listItem.distance = distance.toDouble()

                                    listModel.searchList.add(listItem)



                                }


                            }


                        }

                    }


                }
            }catch (ex:Exception){

            }
            return listModel
        }

        fun getDistanceBetweenTwoLocation(
            centerLatOfSeatle: Double?,
            centerLngOfSeatle: Double?,
            shopLat: Double?,
            shopLong:Double?
        ): Float {
            val startPoint = Location("locationA")
            startPoint.setLatitude(shopLat!!)
            startPoint.setLongitude(shopLong!!)
            val endPoint = Location("locationB")
            endPoint.setLatitude(centerLatOfSeatle!!)
            endPoint.setLongitude(centerLngOfSeatle!!)
            val distance = startPoint.distanceTo(endPoint) / 1000
            return distance
        }


    }


}