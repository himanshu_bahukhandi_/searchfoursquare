package com.himanshu.searchview.gateway.model

data class SearchNetworkResponse(
    val meta: Meta? = Meta(),
    val response: Response? = Response()
)

data class Meta(
    val code: Int? = 0,
    val requestId: String? = ""
)

data class Response(
    val geocode: Geocode? = Geocode(),
    val venues: List<Venue?>? = listOf()
)

data class Venue(
    val beenHere: BeenHere? = BeenHere(),
    val categories: List<Category?>? = listOf(),
    val contact: Contact? = Contact(),
    val hasPerk: Boolean? = false,
    val hereNow: HereNow? = HereNow(),
    val id: String? = "",
    val location: Location? = Location(),
    val name: String? = "",
    val referralId: String? = "",
    val stats: Stats? = Stats(),
    val venueChains: List<Any?>? = listOf(),
    val venuePage: VenuePage? = VenuePage(),
    val verified: Boolean? = false
)

data class Location(
    val address: String? = "",
    val cc: String? = "",
    val city: String? = "",
    val country: String? = "",
    val crossStreet: String? = "",
    val formattedAddress: List<String?>? = listOf(),
    val labeledLatLngs: List<LabeledLatLng?>? = listOf(),
    val lat: Double? = 0.0,
    val lng: Double? = 0.0,
    val postalCode: String? = "",
    val state: String? = ""
)

data class LabeledLatLng(
    val label: String? = "",
    val lat: Double? = 0.0,
    val lng: Double? = 0.0
)

data class BeenHere(
    val count: Int? = 0,
    val lastCheckinExpiredAt: Int? = 0,
    val marked: Boolean? = false,
    val unconfirmedCount: Int? = 0
)

class Contact(
)

data class HereNow(
    val count: Int? = 0,
    val groups: List<Any?>? = listOf(),
    val summary: String? = ""
)

data class Category(
    val icon: Icon? = Icon(),
    val id: String? = "",
    val name: String? = "",
    val pluralName: String? = "",
    val primary: Boolean? = false,
    val shortName: String? = ""
)

data class Icon(
    val prefix: String? = "",
    val suffix: String? = ""
)

data class VenuePage(
    val id: String? = ""
)

data class Stats(
    val checkinsCount: Int? = 0,
    val tipCount: Int? = 0,
    val usersCount: Int? = 0,
    val visitsCount: Int? = 0
)

data class Geocode(
    val `where`: String? = "",
    val feature: Feature? = Feature(),
    val parents: List<Any?>? = listOf(),
    val what: String? = ""
)

data class Feature(
    val cc: String? = "",
    val displayName: String? = "",
    val geometry: Geometry? = Geometry(),
    val highlightedName: String? = "",
    val id: String? = "",
    val longId: String? = "",
    val matchedName: String? = "",
    val name: String? = "",
    val slug: String? = "",
    val woeType: Int? = 0
)

data class Geometry(
    val bounds: Bounds? = Bounds(),
    val center: Center? = Center()
)

data class Center(
    val lat: Double? = 0.0,
    val lng: Double? = 0.0
)

data class Bounds(
    val ne: Ne? = Ne(),
    val sw: Sw? = Sw()
)

data class Sw(
    val lat: Double? = 0.0,
    val lng: Double? = 0.0
)

data class Ne(
    val lat: Double? = 0.0,
    val lng: Double? = 0.0
)