package com.himanshu.searchview.gateway

import android.arch.lifecycle.MutableLiveData
import com.himanshu.searchview.gateway.local.SearchLocalDataSource
import com.himanshu.searchview.gateway.remote.SearchRemoteDataSource
import com.himanshu.searchview.network.Response
import com.himanshu.searchview.ui.model.SearchListArrayModel
import io.reactivex.observers.DisposableObserver


open class SearchRepository(var local: SearchLocalDataSource, var remote: SearchRemoteDataSource) {

    var loaderLiveData = MutableLiveData<Boolean>()

    fun getSearchResults(queryText: String): MutableLiveData<Response<SearchListArrayModel>> {
        loaderLiveData.postValue(true)
        var response = MutableLiveData<Response<SearchListArrayModel>>()
        var localData = local.getSearchResults(queryText)


        localData.subscribeWith(object : DisposableObserver<Response<SearchListArrayModel>>() {
            override fun onComplete() {

            }

            override fun onNext(t: Response<SearchListArrayModel>) {

                if (t.getStatus() == Response.Status.SUCCESS) {
                    loaderLiveData.postValue(false)
                    response.postValue(t)
                } else {
                    var remoteData = remote.getSearchResults(queryText)

                    remoteData.subscribeWith(object : DisposableObserver<Response<SearchListArrayModel>>() {
                        override fun onComplete() {
                        }

                        override fun onNext(t: Response<SearchListArrayModel>) {
                            if (t.getStatus() == Response.Status.SUCCESS) {
                                local.saveResultData(queryText,t.data!!)
                            }
                            loaderLiveData.postValue(false)
                            response.postValue(t)

                        }

                        override fun onError(e: Throwable) {
                        }


                    })

                    /*remoteData.subscribe { t: Response<SearchListArrayModel> ->
                        {
                            if (t.getStatus() == Response.Status.SUCCESS) {
                                local.saveResultData(t.data!!)
                            }
                            loaderLiveData.postValue(false)
                            response.postValue(t)
                        }
                    }*/
                }
            }


            override fun onError(e: Throwable) {
            }


        })
        /*localData.subscribe { t: Response<SearchListArrayModel> ->
            {
                if (t.getStatus() == Response.Status.SUCCESS) {
                    loaderLiveData.postValue(false)
                    response.postValue(t)
                } else {
                    var remoteData = remote.getSearchResults(queryText)
                    remoteData.subscribe { t: Response<SearchListArrayModel> ->
                        {
                            if (t.getStatus() == Response.Status.SUCCESS) {
                                local.saveResultData(t.data!!)
                            }
                            loaderLiveData.postValue(false)
                            response.postValue(t)
                        }
                    }
                }
            }
        }*/
        return response;


    }


}