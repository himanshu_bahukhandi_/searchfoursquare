package com.himanshu.searchview.network

import com.himanshu.searchview.gateway.model.SearchNetworkResponse
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("/v2/venues/search")
    abstract fun searchItem(
        @Query("client_id") clientId: String,
        @Query("client_secret") clientSecret: String,
        @Query("near") near: String,
        @Query("query") query: String,
        @Query("v") v: String,
        @Query("limit") limit: Int
    ): Observable<SearchNetworkResponse>
}