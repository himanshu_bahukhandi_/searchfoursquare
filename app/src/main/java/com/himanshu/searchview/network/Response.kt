package com.himanshu.searchview.network

import com.google.gson.Gson

class Response<T> private constructor(
    private val status: Status, val data: T?,
    val throwable: CommonAPIError?
) {

    //    public static <T> Response<T> loading(@Nullable T data) {
    //        return new Response<>(Status.LOADING, data, null);
    //    }

    enum class Status {
        SUCCESS, ERROR
    }

    fun getStatus():Status = status

    companion object {

        fun <T> success(data: T): Response<T> {
            return Response(Status.SUCCESS, data, null)
        }



        fun <T1> error(throwable: Throwable, response: retrofit2.Response<T1>?, T: Class<T1>): Response<T1> {
            val errorCode = getErrorCode<T1>(response)
            if (response != null && response.errorBody() != null) {
                try {
                    val resData = response.errorBody().string()
                    val t = Gson().fromJson<T1>(resData, T)
                    return Response<T1>(Status.ERROR, t, CommonAPIError(errorCode))
                } catch (e: Exception) {
                    return Response<T1>(Status.ERROR, null, CommonAPIError(errorCode, e.message))
                }

            }
            return Response<T1>(Status.ERROR, null, CommonAPIError(errorCode, throwable.message))
        }

        fun <T2> error():Response<T2>{
            return Response<T2>(Status.ERROR, null, CommonAPIError(101, "Error"))
        }

        private fun <T> getErrorCode(response: retrofit2.Response<T>?): Int {
            try {
                return response!!.raw().code()
            } catch (e: NullPointerException) {
                return -1
            }
        }

    }
}