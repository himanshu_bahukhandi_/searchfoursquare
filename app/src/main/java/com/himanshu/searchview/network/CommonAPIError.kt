package com.himanshu.searchview.network

import android.content.Context
import com.himanshu.searchview.R

import com.himanshu.searchview.ui.BaseActivity
import java.net.SocketTimeoutException
import java.net.UnknownHostException

import retrofit2.Response


class CommonAPIError @JvmOverloads constructor(var responseCode: Int, var message1: String? = null) : Exception() {
    companion object {

        fun getErrorMessage(context: Context, error: CommonAPIError?): String? {
            return if (error == null) {
                context.getString(R.string.msg_something_went_wrong)
            } else getErrorMessage(context, error.responseCode, error)
        }

        fun getErrorMessage(context: Context, errorCode: Int, throwable: Throwable?): String? {
            var errorMsg: String? = null
            when (errorCode) {
                400 -> errorMsg = context.getString(R.string.msg_something_went_wrong)
                401, 410 -> errorMsg = context.getString(R.string.msg_something_went_wrong)
                403 -> {
                    errorMsg = context.getString(R.string.msg_something_went_wrong)
                    return errorMsg
                }
                404 -> errorMsg = context.getString(R.string.msg_something_went_wrong)
                408 -> errorMsg = context.getString(R.string.msg_something_went_wrong)
                429 -> errorMsg = context.getString(R.string.msg_something_went_wrong)
                500 -> errorMsg = context.getString(R.string.msg_something_went_wrong)
                503 -> errorMsg = context.getString(R.string.msg_something_went_wrong)
            }//            default:
            //                errorMsg = context.getString(R.string.msg_something_went_wrong);
            //                break;
            if (errorMsg != null) {
                if (errorCode != 401 && errorCode != 410)
                    errorMsg = errorMsg + " " + context.getString(R.string.msg_something_went_wrong)
                return errorMsg
            } else {
                errorMsg = context.getString(R.string.msg_something_went_wrong)
            }

            if (throwable != null) {
                if (throwable is UnknownHostException) {
                    errorMsg = context.getString(R.string.msg_something_went_wrong)
                } else if (throwable is SocketTimeoutException) {
                    errorMsg = context.getString(R.string.msg_something_went_wrong)
                } else {
                    errorMsg = context.getString(R.string.msg_something_went_wrong)
                }
            }
            return errorMsg
        }

    }

}


