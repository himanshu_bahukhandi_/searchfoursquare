package com.himanshu.searchview

import android.app.Application
import android.content.Context
import com.himanshu.searchview.di.component.AppComponent
import com.himanshu.searchview.di.component.DaggerAppComponent
import com.himanshu.searchview.di.module.AppModule
import com.himanshu.searchview.di.module.NetworkModule
import com.himanshu.searchview.di.module.SearchModule

open class SearchApplication : Application() {
    private var component: AppComponent? = null
    companion object{
        var context:Context?=null;

    }

    override fun onCreate() {
        super.onCreate()
        context=this;
        component = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .searchModule(SearchModule())
            .networkModule(NetworkModule())
            .build()

    }


    fun getComponent(): AppComponent {
        return component!!
    }


}