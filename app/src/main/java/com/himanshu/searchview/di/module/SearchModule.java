package com.himanshu.searchview.di.module;

import com.himanshu.searchview.network.SearchApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 *
 */
@Module
public class SearchModule {

    @Provides
    @Singleton
    SearchApi provideRetrofit(Retrofit retrofit) {
        return retrofit.create(SearchApi.class);
    }

}
