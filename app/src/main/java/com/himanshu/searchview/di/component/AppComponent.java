package com.himanshu.searchview.di.component;

import com.himanshu.searchview.di.module.AppModule;
import com.himanshu.searchview.di.module.SearchModule;
import com.himanshu.searchview.gateway.remote.SearchRemoteDataSource;
import com.himanshu.searchview.ui.BaseActivity;
import com.himanshu.searchview.ui.LocationDetailActivity;
import com.himanshu.searchview.ui.MainActivity;
import com.himanshu.searchview.di.module.NetworkModule;

import javax.inject.Inject;
import javax.inject.Singleton;


import com.himanshu.searchview.ui.adapter.SearchListAdapter;
import dagger.Component;

/**
 *
 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, SearchModule.class})
public interface AppComponent {

    void inject(BaseActivity activity);

    void inject(LocationDetailActivity activity);

    void inject(SearchRemoteDataSource remoteDataSource);

    void inject(SearchListAdapter adapter);

}
