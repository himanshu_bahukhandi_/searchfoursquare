package com.himanshu.searchview.ui.listener

interface ActionListener {

    fun onAction(reqId:Int,data:Any?)
    fun getDataFromSource(reqId:Int,data:Any?):Any?
}