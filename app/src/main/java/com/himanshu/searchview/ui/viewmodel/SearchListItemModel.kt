package com.himanshu.searchview.ui.viewmodel

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.text.TextUtils
import com.himanshu.searchview.ui.model.SearchListItem


open class SearchListItemModel:BaseObservable(){
    var dataModel: SearchListItem? = null

    fun DataItemViewModel(dataModel: SearchListItem) {
        this.dataModel = dataModel
    }

    fun setUp() {
        // perform set up tasks, such as adding listeners
    }

    fun tearDown() {
        // perform tear down tasks, such as removing listeners
    }

    @Bindable
    fun getName(): String {
        return if (!TextUtils.isEmpty(dataModel!!.name)) dataModel!!.name else ""
    }

    @Bindable
    fun getCategory(): String {
        return if (!TextUtils.isEmpty(dataModel!!.category)) dataModel!!.category else ""
    }

    @Bindable
    fun getCategoryIcon(): String {
        return if (!TextUtils.isEmpty(dataModel!!.categoryIcon)) dataModel!!.categoryIcon else ""
    }


    @Bindable
    fun getFavIcon(): String {
        return if (!TextUtils.isEmpty(dataModel!!.categoryIcon)) dataModel!!.categoryIcon else ""
    }



}