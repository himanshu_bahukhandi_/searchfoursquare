package com.himanshu.searchview.ui.fragment

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.himanshu.searchview.R
import com.himanshu.searchview.ui.utils.Utils
import kotlinx.android.synthetic.main.fragment_search.*
import android.widget.ArrayAdapter
import android.widget.AdapterView
import android.content.Context.INPUT_METHOD_SERVICE
import android.support.v4.content.ContextCompat.getSystemService
import android.view.inputmethod.InputMethodManager


open class SearchFragment : BaseFragment(), AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {

    val list: List<String> = listOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(com.himanshu.searchview.R.layout.fragment_search, container, false)
        return view;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }


    private fun initView() {
        setUpAutocomplete()


        submit_bt.setOnClickListener({

            if (TextUtils.isEmpty(search_et.text.toString())) {

                Toast.makeText(activity, "Please enter valid value", Toast.LENGTH_SHORT).show()


            } else {
                openSearchList(search_et.text.toString())
            }

        })


    }

    private fun SearchFragment.setUpAutocomplete() {
        val list = Utils.getLastSearchedValue();
        //Create adapter
        val adapter = ArrayAdapter<String>(activity, android.R.layout.simple_dropdown_item_1line, list)

        search_et.setThreshold(1)
        //Set adapter to AutoCompleteTextView
        search_et.setAdapter(adapter)
        search_et.setOnItemSelectedListener(this)
        search_et.setOnItemClickListener(this)
    }

    private fun openSearchList(queryText: String) {
        if (Utils.isNetworkAvailable(activity)) {
            var searchListFragment = SearchListFragment()
            Utils.saveInLastSearchedValues(search_et.text.toString())
            var bundle = Bundle()
            bundle.putString("query", queryText)
            searchListFragment.arguments = bundle
            activity!!.supportFragmentManager.beginTransaction()
                .replace(com.himanshu.searchview.R.id.container, searchListFragment)
                .addToBackStack(null).commitAllowingStateLoss()
        } else {
            Toast.makeText(activity, "Network not available", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onItemSelected(
        arg0: AdapterView<*>, arg1: View, position: Int,
        arg3: Long
    ) {
        openSearchList(list.get(position))
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {

        val imm = activity!!.getSystemService(
            INPUT_METHOD_SERVICE
        ) as InputMethodManager?
        imm!!.hideSoftInputFromWindow(activity!!.getCurrentFocus().getWindowToken(), 0)

    }

    override fun onItemClick(arg0: AdapterView<*>, arg1: View, position: Int, arg3: Long) {
        if (list.size > position)
            openSearchList(list.get(position))

    }

}