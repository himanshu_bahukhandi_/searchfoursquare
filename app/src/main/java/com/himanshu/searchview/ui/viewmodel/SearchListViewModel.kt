package com.himanshu.searchview.ui.viewmodel

import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.himanshu.searchview.SearchApplication
import com.himanshu.searchview.gateway.SearchRepository
import com.himanshu.searchview.gateway.local.SearchLocalDataSource
import com.himanshu.searchview.gateway.remote.SearchRemoteDataSource
import com.himanshu.searchview.network.Response
import com.himanshu.searchview.ui.listener.ActionListener
import com.himanshu.searchview.ui.model.SearchListArrayModel
import com.himanshu.searchview.ui.model.SearchListItem
import com.himanshu.searchview.ui.utils.UIConstants

open class SearchListViewModel(): ViewModel(),ActionListener{

    var searchList:ArrayList<SearchListItem> = ArrayList();


    override fun onAction(reqId: Int, data: Any?) {

    }

    override fun getDataFromSource(reqId: Int,data:Any?): Any? {
        if(reqId==UIConstants.REQ_LIST_COUNT && data==null) {
            return searchList.size as Any
        }
        else{
            return searchList.get(data as Int) as Any
        }


    }


    fun setData(data:ArrayList<SearchListItem>){
        searchList.addAll(data)
    }


    public fun hitApi(query:String): LiveData<Response<SearchListArrayModel>> {

        var repo=SearchRepository(SearchLocalDataSource(), SearchRemoteDataSource())
        var observable:MutableLiveData<Response<SearchListArrayModel>> = repo.getSearchResults(query)
        return observable

    }



}