package com.himanshu.searchview.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.himanshu.searchview.R
import com.himanshu.searchview.ui.fragment.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar.setTitle("Four Square Search")
        addFragment(R.id.container,SearchFragment())


    }
}
