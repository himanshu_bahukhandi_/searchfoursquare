package com.himanshu.searchview.ui.model

import com.himanshu.searchview.network.IResponseModel

class SearchListArrayModel:IResponseModel {

    var searchList:ArrayList<SearchListItem> = ArrayList();

}