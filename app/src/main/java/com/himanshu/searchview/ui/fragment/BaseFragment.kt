package com.himanshu.searchview.ui.fragment

import android.app.ProgressDialog
import android.content.Context
import android.support.v4.app.Fragment

open class BaseFragment:Fragment() {

    protected var mProgressDialog: ProgressDialog? = null

    protected fun showProgressDialog(context: Context?, message: String) {
        if (activity == null || !isAdded) {
            return
        }

        if (context == null && !activity!!.isFinishing)
            return

        if (mProgressDialog != null && mProgressDialog!!.isShowing()) {
            mProgressDialog!!.setMessage(message)
            return
        }
        mProgressDialog = ProgressDialog(context)

        try {
            // mProgressDialog.setIcon(R.drawable.paytm_logo);
            mProgressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mProgressDialog!!.setMessage(message)
            mProgressDialog!!.setCancelable(true)
            mProgressDialog!!.setCanceledOnTouchOutside(true)
            mProgressDialog!!.show()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
            /*
             * When we dismiss dialog and finish the activity, sometimes
             * activity get finished before dialog successfully dismisses. So
             * that dialog is no longer attached to the view of destroyed
             * activity.
             */
        } catch (e: Exception) {
            /*
             * When we dismiss dialog and finish the activity, sometimes
             * activity get finished before dialog successfully dismisses. So
             * that dialog is no longer attached to the view of destroyed
             * activity.
             */
        }

    }

    protected fun removeProgressDialog() {
        try {
            if (activity == null || isDetached) {
                return
            }

            if (mProgressDialog != null &&  mProgressDialog!!.isShowing() && !activity!!.isFinishing) {
                mProgressDialog!!.dismiss()
                mProgressDialog = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}