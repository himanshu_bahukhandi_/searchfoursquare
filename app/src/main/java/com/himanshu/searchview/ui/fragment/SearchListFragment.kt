package com.himanshu.searchview.ui.fragment

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.himanshu.searchview.R
import com.himanshu.searchview.network.Response
import com.himanshu.searchview.ui.LocationDetailActivity
import com.himanshu.searchview.ui.adapter.SearchListAdapter
import com.himanshu.searchview.ui.listener.ActionListener
import com.himanshu.searchview.ui.model.SearchListArrayModel
import com.himanshu.searchview.ui.model.SearchListItem
import com.himanshu.searchview.ui.utils.UIConstants
import com.himanshu.searchview.ui.viewmodel.SearchListViewModel
import kotlinx.android.synthetic.main.fragment_search_results.*

open class SearchListFragment : BaseFragment(), ActionListener {
    override fun onAction(reqId: Int, data: Any?) {
        if (reqId == UIConstants.REQ_ITEM_CLICKED) {
            var map: HashMap<String, Any> = data as HashMap<String, Any>
            var intent = Intent(activity, LocationDetailActivity::class.java)
            intent.putExtra(UIConstants.SEARCH_ITEM, map.get(UIConstants.SEARCH_ITEM) as SearchListItem)
            intent.putExtra(UIConstants.ITEM_POS, map.get(UIConstants.ITEM_POS) as Int)

            startActivityForResult(intent, 123)
        }
    }

    override fun getDataFromSource(reqId: Int, data: Any?): Any? {
        return null;
    }

    private lateinit var adapter: SearchListAdapter
    var viewModel: SearchListViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_search_results, container, false)
        viewModel = ViewModelProviders.of(this).get(SearchListViewModel::class.java)
        return view;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }


    private fun initView() {

        search_result_rv.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        adapter = SearchListAdapter(activity as Context, viewModel!!, this)
        search_result_rv.adapter = adapter
        showProgressDialog(activity,"Please wait")
        var observable = viewModel!!.hitApi(arguments!!.getString("query"))

        observable.observe(this, object : Observer<Response<SearchListArrayModel>> {
            override fun onChanged(t: Response<SearchListArrayModel>?) {
                removeProgressDialog()
                if (t!!.getStatus() == Response.Status.SUCCESS) {
                    viewModel!!.setData(t!!.data!!.searchList)
                    adapter.notifyDataSetChanged()
                } else {
                    Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
                }
            }


        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 123 && resultCode == Activity.RESULT_OK) {
            adapter.notifyItemChanged(data!!.getIntExtra(UIConstants.ITEM_POS, 0))
        }
    }
}

