package com.himanshu.searchview.ui.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class SearchListItem(

    var distance: Double = 0.0,
    var id:String="",
    var name:String="",
    var category:String="",
    var categoryIcon:String="",
    var lat:Double = 0.0,
    var long:Double = 0.0,
    var isFav:Boolean=false,
    var address:String=""
):Parcelable

