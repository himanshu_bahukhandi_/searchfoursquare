package com.himanshu.searchview.ui

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.himanshu.searchview.SearchApplication
import com.himanshu.searchview.di.component.AppComponent
import javax.inject.Inject


public abstract class BaseActivity : AppCompatActivity() {


    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.getApplicationComponent().inject(this)
    }

    /**
     * Adds a [Fragment] to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment The fragment to be added.
     */
    protected fun addFragment(containerViewId: Int, fragment: Fragment) {
        val fragmentTransaction = this.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(containerViewId, fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }

    /**
     * Get the Main Application component for dependency injection.
     *
     * @return [com.fernandocejas.android10.sample.presentation.internal.di.components.ApplicationComponent]
     */
    protected fun getApplicationComponent(): AppComponent {
        return (getApplication() as SearchApplication).getComponent()
    }


}
