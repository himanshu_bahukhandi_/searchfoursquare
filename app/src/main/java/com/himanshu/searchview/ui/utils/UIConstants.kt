package com.himanshu.searchview.ui.utils

open class UIConstants {


    companion object {

        val LAST_SEARCHED: String="last_searched"
        val REQ_LIST_COUNT = 123;
        val REQ_GET_LIST_ITEM = 124;
        val REQ_ITEM_CLICKED = 125;
        val CENTRE_LAT = "latitude"
        val CENTRE_LNG = "longitude"
        val SEARCH_ITEM="item"
        val ITEM_POS="item_pos"

    }


}