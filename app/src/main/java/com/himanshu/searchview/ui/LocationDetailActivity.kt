package com.himanshu.searchview.ui

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.content.Context
import android.content.Intent
import android.os.Handler
import com.himanshu.searchview.SearchApplication
import com.himanshu.searchview.ui.model.SearchListItem
import com.himanshu.searchview.ui.utils.SharedPrefUtils
import com.himanshu.searchview.ui.utils.UIConstants
import android.util.TypedValue
import android.widget.RelativeLayout
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.himanshu.searchview.ui.fragment.DetailFragment
import kotlinx.android.synthetic.main.activity_detail_page.*
import com.himanshu.searchview.ui.adapter.ViewPagerAdapter
import android.R
import android.support.design.widget.CollapsingToolbarLayout






open class LocationDetailActivity : AppCompatActivity(), OnMapReadyCallback {
    private var map: GoogleMap? = null
    private var item: SearchListItem? = null;
    private var position: Int = 0
    private var handler=Handler()

    override fun onMapReady(googleMap: GoogleMap?) {

        map = googleMap

        map!!.uiSettings.setAllGesturesEnabled(true)
        map!!.uiSettings.isZoomControlsEnabled = true
        map!!.uiSettings.isScrollGesturesEnabled=true
        map!!.uiSettings.isScrollGesturesEnabledDuringRotateOrZoom=true
        val CENTRE = LatLng(
            SharedPrefUtils.getStringData(
                SearchApplication.context as Context,
                UIConstants.CENTRE_LAT
            )!!.toDouble(),
            SharedPrefUtils.getStringData(SearchApplication.context as Context, UIConstants.CENTRE_LNG)!!.toDouble()
        )
        val SHOPLOCATION = LatLng(item!!.lat, item!!.long)

        /*val CENTRE = LatLng(
            47.6219,
            122.3517
        )
*/



        if (map != null) {
            val hamburg = map!!.addMarker(
                MarkerOptions()
                    .position(CENTRE)
                    .title("Seatle Center")
                    .snippet("Seatle")
                    .icon(
                        BitmapDescriptorFactory
                            .fromResource(com.himanshu.searchview.R.drawable.pin)
                    )
            )
            val kiel = map!!.addMarker(
                MarkerOptions()
                    .position(SHOPLOCATION)
                    .title(item!!.name)
                    .snippet("Good Location")
                    .icon(
                        BitmapDescriptorFactory
                            .fromResource(com.himanshu.searchview.R.drawable.pin)
                    )
            )
        }
        map!!.moveCamera(CameraUpdateFactory.newLatLng(CENTRE));
      /*  var builder = LatLngBounds.builder();

        builder.include(CENTRE);
        builder.include(SHOPLOCATION)

        var bounds = builder.build();
        map!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100,100,100))*/
       /* handler.postDelayed({
            map!!.moveCamera(CameraUpdateFactory.zoomTo(8f));
        },500)*/


    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.himanshu.searchview.R.layout.activity_detail_page)


        collapsing_toolbar.title = "Details"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        getValuesFromIntent()
        var mapFragment = (supportFragmentManager.findFragmentById(com.himanshu.searchview.R.id.map) as SupportMapFragment)
        mapFragment.getMapAsync(this)
        val adapter = ViewPagerAdapter(
            supportFragmentManager
        )
        var fragment=DetailFragment();
        var bundle=Bundle()
        bundle.putParcelable(UIConstants.SEARCH_ITEM,item)
        fragment.arguments=bundle
        adapter.addFrag(fragment,"Detail")
        viewpager.setAdapter(adapter);

    }

    private fun getValuesFromIntent() {
        item = intent.getParcelableExtra(UIConstants.SEARCH_ITEM)
        position = intent.getIntExtra(UIConstants.ITEM_POS,0)

         }


    override fun onBackPressed() {
        var intent=Intent()
        intent.putExtra(UIConstants.ITEM_POS,getIntent().getIntExtra(UIConstants.ITEM_POS,0))
        setResult(Activity.RESULT_OK,intent)
        finish()
    }

}