package com.himanshu.searchview.ui.utils

import android.content.Context
import android.net.ConnectivityManager
import com.himanshu.searchview.SearchApplication
import java.math.RoundingMode
import java.text.DecimalFormat

open class Utils{


    companion object{

        fun getDistanceFormat(distance:Double):String{


            val num = distance
            val df = DecimalFormat("#.###")
            df.roundingMode = RoundingMode.CEILING

            return df.format(num)+" km";
        }

        fun getLastSearchedValue():List<String>?{
            var list= listOf<String>()
            val str=SharedPrefUtils.getStringData(SearchApplication.context as Context,UIConstants.LAST_SEARCHED)
            if(str!=null) {
                list = str.split("||")
            }
           return list;
        }

        fun saveInLastSearchedValues(query:String){
            val str=SharedPrefUtils.getStringData(SearchApplication.context as Context,UIConstants.LAST_SEARCHED)
            if(str!=null) {


                    var list = str.split("||")
                    var isAlreadyPresent=false;
                    for(str in list){
                        if(query.equals(str,ignoreCase = true)){
                            isAlreadyPresent=true
                            break
                        }
                    }




                if(!isAlreadyPresent) {
                    val newVal = str + "||" + query
                    SharedPrefUtils.saveData(SearchApplication.context as Context, UIConstants.LAST_SEARCHED, newVal)
                }
            }
            else{
                SharedPrefUtils.saveData(SearchApplication.context as Context,UIConstants.LAST_SEARCHED,query)
            }

        }


        fun isNetworkAvailable(context: Context?): Boolean {
            if (context == null) {
                return false
            } else {
                val ConnectMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                if (ConnectMgr == null) {
                    return false
                } else {
                    val NetInfo = ConnectMgr.activeNetworkInfo
                    return NetInfo?.isConnected ?: false
                }
            }
        }
    }


}