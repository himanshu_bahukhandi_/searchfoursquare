package com.himanshu.searchview.ui.adapter

import android.support.v7.widget.RecyclerView
import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.himanshu.searchview.R
import com.himanshu.searchview.SearchApplication
import com.himanshu.searchview.ui.listener.ActionListener
import com.himanshu.searchview.ui.model.SearchListItem
import com.himanshu.searchview.ui.utils.SharedPrefUtils
import com.himanshu.searchview.ui.utils.UIConstants
import com.himanshu.searchview.ui.utils.Utils


open class SearchListAdapter() : RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {

    var viewModel: ActionListener? = null
    var context: Context? = null;
    var uiListener:ActionListener?=null

    constructor(context: Context, viewModel: ActionListener,uiListener:ActionListener) : this() {
        this.viewModel = viewModel
        this.context = context
        this.uiListener=uiListener
        (context.applicationContext as SearchApplication).getComponent().inject(this)

    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val viewHolder = ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_search_list, parent, false)
        )
        return viewHolder

    }

    override fun onBindViewHolder(viewholder: ViewHolder, position: Int) {
        var dataModel =
            viewModel!!.getDataFromSource(UIConstants.REQ_GET_LIST_ITEM, position as Object) as SearchListItem
        viewholder.nameTV.setText(dataModel.name)
        viewholder.categoryTv.setText(dataModel.category)
        viewholder.distanceTv.setText("Distance:"+Utils.getDistanceFormat(dataModel.distance))
        Glide.with(context!!).load(dataModel.categoryIcon).into(viewholder.categoryIV);
        if (SharedPrefUtils.getBooleanData(context!!, dataModel.id)) {
            viewholder.favIv.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_fav))
        } else {
            viewholder.favIv.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_unfav))
        }

    }


    override fun getItemCount(): Int {
        return viewModel!!.getDataFromSource(UIConstants.REQ_LIST_COUNT, null) as Int
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTV: TextView
        var categoryTv: TextView
        var categoryIV: ImageView
        var favIv: ImageView
        var distanceTv: TextView

        init {

            nameTV = itemView.findViewById(R.id.name_tv)
            categoryTv = itemView.findViewById(R.id.category_tv)
            categoryIV = itemView.findViewById(R.id.category_iv)
            favIv = itemView.findViewById(R.id.favorite_iv)
            distanceTv = itemView.findViewById(R.id.distance_tv)
            itemView.setOnClickListener({
                if(uiListener!=null) {
                    var dataModel=viewModel!!.getDataFromSource(UIConstants.REQ_GET_LIST_ITEM,adapterPosition as Object) as SearchListItem
                    var map=HashMap<String,Any>()
                    map.put(UIConstants.SEARCH_ITEM,dataModel)
                    map.put(UIConstants.ITEM_POS,adapterPosition)
                    uiListener!!.onAction(UIConstants.REQ_ITEM_CLICKED, map as Object)
                }


            })

            /*favIv.setOnClickListener(object:View.OnClickListener{
                override fun onClick(v: View?) {
                    var dataModel=viewModel!!.getDataFromSource(adapterPosition) as SearchListItem
                    val isFav=SharedPrefUtils.getBooleanData(context!!,dataModel.id)
                    if(isFav){
                        favIv.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_unfav))
                    }else{
                        favIv.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_fav))
                    }
                    SharedPrefUtils.saveData(context!!,dataModel.id,!isFav)
                }

            })*/


        }
    }
}