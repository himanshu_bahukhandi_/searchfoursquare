package com.himanshu.searchview.ui.fragment

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.himanshu.searchview.R
import com.himanshu.searchview.ui.model.SearchListItem
import com.himanshu.searchview.ui.utils.SharedPrefUtils
import com.himanshu.searchview.ui.utils.UIConstants
import com.himanshu.searchview.ui.utils.Utils
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_search.*

open class DetailFragment : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        return view;
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }



    private fun initView(){

        var item=arguments!!.getParcelable(UIConstants.SEARCH_ITEM) as SearchListItem
        name_tv.setText(item.name)
        distance_tv.setText(Utils.getDistanceFormat(item.distance))
        address_tv.setText(item.address)
        if (SharedPrefUtils.getBooleanData(context!!, item.id)) {
            fav_iv.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_fav))
        } else {
            fav_iv.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_unfav))
        }

        fav_iv.setOnClickListener({
            val isFav=SharedPrefUtils.getBooleanData(context!!,item.id)
            if(isFav){
                fav_iv.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_unfav))
            }else{
                fav_iv.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_fav))
            }
            SharedPrefUtils.saveData(context!!,item.id,!isFav)

     })



    }

}