

## Following Libraries are used while creating this project


1. Rxjava
2. Retrofit for networking
3. Dagger for dependency injection(Used at very few places just to show the usage)
4. Android support library
5. JUnit for test cases(Written few cases to show usage)
6. LiveData with ViewModel


---



## Features


1.Request Cache(LRU) is used for fetching last fetched values again

2- Shared Prefs has been used as local storage for hints in next run

3-Details page is created for demo so it can be extended for all other details(Now only few details shown)

4-Favorites are stored in shared prefs

5-AutoComplete for search

6-ViewModel is used to handle configuration changes
 
7-Google map is shown but pinch to zoom not working due to collapsingtoolbar layout issue taking all touches before google map takes it.

8-Test cases are written for null checks in api



## Issues need to be fixed


1.Google map is not taking touches due to collapsing toolbar , These is design issue and need to fixed by implementing different UI.

2-Due to 1st issue map is showing not zoom up and single marker is showing. You need to zoom in to see second marker 





## Clone a repository


1. After cloning please sync with gradle to make the project load its settings
